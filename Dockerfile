FROM jenkins/jenkins:lts
MAINTAINER Larry SU <larrysu1115@gmail.com>

USER root

RUN cd ~ \
    && curl -sL https://deb.nodesource.com/setup_8.x -o nodesource_setup.sh \
    && bash nodesource_setup.sh \
    && apt-get -y --no-install-recommends install nodejs \
    && apt-get -y --no-install-recommends install build-essential

RUN apt-get -y --no-install-recommends install vim \
    && apt-get -y --no-install-recommends install netcat \
    && apt-get -y --no-install-recommends install net-tools

USER jenkins
