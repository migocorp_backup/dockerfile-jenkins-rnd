# jenkins-rnd

Jenkins build with current RnD toolkits

```
# run jenkins container
docker run -d \
  --name jenkins-rnd
  -v jenkins_home:/var/jenkins_home \
  -p 8080:8080 -p 50000:50000 \
  migocorp/jenkins-rnd:0.1
```

## To build image

```
docker build -t="migocorp/jenkins-rnd:0.1" .
```
